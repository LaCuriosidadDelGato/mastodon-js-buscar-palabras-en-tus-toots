# MASTODON-JS - Buscar palabras en tus toots

Programa simple en JavaScript-HTML5 para poder buscar palabras que aparecen en tus propios toots desde el archivo de copia de seguridad. Util si tu instancia ha capado esa opción. 

Hay que cargar el archivo 'outbox.json'

### Notas de interés

- Busca desde el archivo 'outbox.json' que se puede obtener en la url `https://tuinstancia/settings/export` (cambiar `tuinstancia` por la url de tu propia instancia)
- Es una busqueda muy simple. Si buscas `hola` te dará toots con la palabra `hola` pero también `holahola`, `rehola`... 
- No hace falta un servidor para ejecutarlo
- Si no quieres descargarlo puedes usar la [la versión online](https://mtdn.mcempv.ovh/buscadorToots/v1/) que no guarda el `outbox.json` en el servidor


