
const App = {
    cargarArchivo: (input)=>{
        let file = input.files[0];
        App.reader = new FileReader();
      
        App.reader.addEventListener("load", () => {
                document.getElementById("proceso").innerHTML = 'Cargado 100%';
                App.resultado = JSON.parse(App.reader.result);
                console.log(App.resultado);
            }, false,
        );

        App.reader.addEventListener("progress", (e) => {
                document.getElementById("proceso").innerHTML = 'Cargado '+Math.floor(e.loaded*100/e.total)+'%';
            },false,
        );
      
        if (file) {
            App.reader.readAsText(file);
        }
    },

    _buscar(palabras, id){
        for(let i=0;i<palabras.length;i++){
            if(App.resultado.orderedItems[id].object.content.search(palabras[i]) != -1){
                return true;
            }
        }

        return false;
    },

    buscar: (e)=>{
        let palabras = document.getElementById("campoTexto").value;
        let cant = App.resultado.orderedItems.length;
        let toots = [];

        palabras = palabras.split(' ');

        document.getElementById("div").innerHTML = '';

        for(let i=0;i<cant;i++){
            if(!(i%1000)){
                //console.log(i,cant,'Leído '+(Math.floor(i*100/cant))+'%')
                document.getElementById("proceso2").innerHTML = 'Leído '+(Math.floor(i*100/cant))+'%';
            }
            
            if(App.resultado.orderedItems[i].object.content!==undefined){
                if(App._buscar(palabras, i)){
                    toots.push('<a href="'+App.resultado.orderedItems[i].object.atomUri+'" target="_blank">'+App.resultado.orderedItems[i].object.atomUri+'</a><br>'+
                    App.resultado.orderedItems[i].object.content);
                }
            }
        }
        
        document.getElementById("proceso2").innerHTML = 'Leído 100%';

        document.getElementById("div").innerHTML = toots.join('<hr>');
    }

}
